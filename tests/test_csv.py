import csv
from fonctions_csv import choix_delimiteur, ouverture_csv, ouverture_dictionnaire, ecriture_csv, 


class TestCopieCSVFunction(unittest.TestCase):

    
    def test_choix_delimiteur(self):
        """
        Test pour input (repris sur internet et adapte)
        """
        input_values = ['*']
        output = ['*']

        def test_input(s):
            output.append(s)
            return input_values.pop(0)
        app.input = test_input
        app.print = lambda s : output.append(s)

        app.main()

        assert output == [
            'Veuillez choisir un délimiteur : ',
        ]


    def test_ouverture_csv(self):
        Type = 'reader'
        fichier = 'auto.csv'
        with open(fichier, newline='') as test_r:
            self.assertEqual(ouverture_csv(fichier,Type),test_r)

        Type = 'writer'
        with open(fichier, 'w', newline='') as test_w:
            self.assertEqual(ouverture_csv(fichier,Type),test_w)


    def test_ouverture_dictionnaire(self):
        Type = 'reader'
        fichier = 'auto.csv'
        delimiteur='|'
        fieldname_init = ['address','carrosserie','categorie', 'couleur', 'cylindree', 'date_immat', 'denomination', 
        'energy', 'firstname', 'immat', 'marque', 'name' , 'places' , 'poids', 'puissance', 'type_variante_version', 'vin']

        with open(fichier, newline='') as test_r:
            dict_r = csv.DictReader(test_r, fieldnames=fieldname, delimiter=delimiteur)
            self.assertEqual(ouverture_dictionnaire(test_w,Type,fieldname,delimiteur),dict_r)
