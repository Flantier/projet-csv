import csv
from fonctions_csv import *

"""
Initialisation des parametres
"""
delimiteur = choix_delimiteur()
fieldname_init = ['address','carrosserie','categorie', 'couleur', 'cylindree', 'date_immat', 'denomination', 
'energy', 'firstname', 'immat', 'marque', 'name' , 'places' , 'poids', 'puissance', 'type_variante_version', 'vin']
fieldname_final= ['addresse_titulaire', 'nom', 'prenom', 'immatriculation', 'date_immatriculation', 'vin', 'marque',
'denomination_commerciale', 'couleur', 'carrosserie', 'categorie', 'cylindre', 'energie', 'places', 'poids', 
'puissance', 'type', 'variante', 'version']

"""
Ouverture des fichiers
"""
csvfile_r = ouverture_csv('auto.csv','reader')
csvfile_w = ouverture_csv('autoV2.csv','writer')

"""
Creation des dictionnaires
"""
reader = ouverture_dictionnaire(csvfile_r,'reader',fieldname_init,'|')
writer = ouverture_dictionnaire(csvfile_w,'writer',fieldname_final,delimiteur)

"""
Ecriture
"""
ecriture_csv(reader, writer, fieldname_init, fieldname_final)

"""
Fermeture des fichiers
"""
fermeture_csv(csvfile_r)
fermeture_csv(csvfile_w)
