import csv

def choix_delimiteur():
    """
    Permet de choisir le delimiteur du fichier que l'on souhaite creer
    return: retourne le delimiteur choisi
    """
    delimiteur = input('Veuillez choisir un délimiteur : ')
    return delimiteur

def ouverture_csv(fichier,Type):
    """
    Permet l'ouverture d'un fichier en lecture ou ecriture
    fichier: fichier a ouvrir
    Type: indique s'il s'agit d'un fichier en lecture ou ecriture
    return: renvoie l'ouverture du fichier
    """   
    if (Type == 'reader'):
        csvfile = open(fichier, newline='')
        return csvfile
    elif (Type == 'writer'):
        csvfile = open(fichier, 'w', newline='')
        return csvfile


def ouverture_dictionnaire(csvfile,Type,fieldname,delimiteur):
    """
    Permet de creer le dictionnaire du fichier csv choisi en ecriture ou en lecture
    csvfile: fichier que l'on souhaite ouvrir
    Type: indique s'il s'agit d'un fichier en lecture ou ecriture
    fieldname: liste indiquant les noms de chaque colonnes
    delimiteur: delimiteur entre chaque attributs du fichier
    return: renvoie le dictionnaire souhaite
    """
    if (Type == 'reader'):
        return csv.DictReader(csvfile, fieldnames=fieldname, delimiter=delimiteur)
    elif (Type == 'writer'):
        return  csv.DictWriter(csvfile, fieldnames=fieldname, delimiter=delimiteur)



def ecriture_csv(reader, writer, fieldname_init, fieldname_final):
    """
    Permet de reecrire un fichier en lecture dans un fichier en ecriture (ici selon la necessite des questions du mini-projet)
    reader: le DictReader du fichier en lecture
    writer: le DictWriter du fichier en ecriture
    fieldname_init: liste indiquant les noms de chaque colonnes (ceux du fichier en lecture)
    fieldname_final: liste indiquant les noms de chaque colonnes (ceux du fichier en ecriture)
    """
    writer.writeheader()
    for row in reader:
        Type =row[fieldname_init[15]]
        variante =row[fieldname_init[15]]
        version =row[fieldname_init[15]]
        writer.writerow({ fieldname_final[0] : row[fieldname_init[0]], fieldname_final[1] : row[fieldname_init[11]], fieldname_final[2] : row[fieldname_init[8]],
            fieldname_final[3] : row[fieldname_init[9]], fieldname_final[4] : row[fieldname_init[5]], fieldname_final[5] : row[fieldname_init[16]], fieldname_final[6] : row[fieldname_init[10]],
            fieldname_final[7] : row[fieldname_init[6]], fieldname_final[8] : row[fieldname_init[3]], fieldname_final[9] : row[fieldname_init[1]], fieldname_final[10] : row[fieldname_init[2]],
            fieldname_final[11] : row[fieldname_init[4]], fieldname_final[12] : row[fieldname_init[7]], fieldname_final[13] : row[fieldname_init[12]], fieldname_final[14] : row[fieldname_init[13]],
            fieldname_final[15] : row[fieldname_init[14]], fieldname_final[16] : Type, fieldname_final[17] : variante, fieldname_final[18] : version })



def fermeture_csv(csv_fichier):
    """
    permet de fermer un fichier ouvert au prealable
    csv_fichier: fichier a fermer
    """
    csv_fichier.close()

